package me.ivanovs.microlendingapp.core.database.hibernate;

import java.util.Date;
import java.util.List;

import me.ivanovs.microlendingapp.core.database.LoanDAO;
import me.ivanovs.microlendingapp.core.domain.Loan;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoanDAOImpl implements LoanDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public long getUserLoansCountAfterDate(String userIP, Date date) {
        Criteria criteria = getCurrentSession().createCriteria(Loan.class);
        criteria.add(Restrictions.eq("userIP", userIP));
        criteria.add(Restrictions.ge("date", date));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }

    @Override
    public void create(Loan loan) {
        getCurrentSession().saveOrUpdate(loan);
    }

    @Override
    public Loan getById(Long id) {
        return (Loan) getCurrentSession().get(Loan.class, id);
    }

    @Override
    public void update(Loan loan) {
        getCurrentSession().saveOrUpdate(loan);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Loan> getAll() {
        Criteria criteria = getCurrentSession().createCriteria(Loan.class);
        criteria.addOrder(Order.asc("date"));
        return criteria.list();
    }
}
