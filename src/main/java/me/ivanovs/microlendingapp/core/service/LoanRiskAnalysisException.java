package me.ivanovs.microlendingapp.core.service;

public class LoanRiskAnalysisException extends Exception {

    private static final long serialVersionUID = 1L;

    public LoanRiskAnalysisException(String message) {
        super(message);
    }

}
