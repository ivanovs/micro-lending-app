package me.ivanovs.microlendingapp.restapi.controllers;

import java.util.Date;

import me.ivanovs.microlendingapp.core.domain.LoanExtension;
import me.ivanovs.microlendingapp.core.service.LoanExtensionCreationException;
import me.ivanovs.microlendingapp.core.service.LoanExtensionService;
import me.ivanovs.microlendingapp.restapi.model.CreateLoanExtensionResult;
import me.ivanovs.microlendingapp.restapi.model.LoanExtensionDTO;
import me.ivanovs.microlendingapp.restapi.model.converters.LoanExtensionDTOConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoanExtensionController {

    @Autowired
    private LoanExtensionDTOConverter extensionDTOConverter;

    @Autowired
    private LoanExtensionService extensionService;

    @RequestMapping(method = RequestMethod.POST, value = "/loan/extend")
    public ResponseEntity<CreateLoanExtensionResult> extendLoan(
            @RequestBody LoanExtensionDTO extensionDTO) {

        LoanExtension extension = extensionDTOConverter.convertFromDTO(extensionDTO);
        extension.setDate(new Date());

        CreateLoanExtensionResult createExtensionResult = new CreateLoanExtensionResult();
        try {
            LoanExtension result = extensionService.extendLoan(extensionDTO.getLoanId());
            LoanExtensionDTO resultDTO = extensionDTOConverter.convertFromModel(result);
            createExtensionResult.setExtensionDTO(resultDTO);
        } catch (LoanExtensionCreationException e) {
            createExtensionResult.setErrorMessage("Error while extending loan: " + e.getMessage());
        }
        return new ResponseEntity<CreateLoanExtensionResult>(createExtensionResult, HttpStatus.CREATED);
    }
}
