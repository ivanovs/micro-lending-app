package me.ivanovs.microlendingapp.restapi.model;

public class CreateLoanExtensionResult extends PostActionResult {

    private LoanExtensionDTO extensionDTO;

    public LoanExtensionDTO getExtensionDTO() {
        return extensionDTO;
    }

    public void setExtensionDTO(LoanExtensionDTO extensionDTO) {
        this.extensionDTO = extensionDTO;
    }

}
