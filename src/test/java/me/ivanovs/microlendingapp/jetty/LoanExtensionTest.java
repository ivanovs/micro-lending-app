package me.ivanovs.microlendingapp.jetty;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.HashMap;

import me.ivanovs.microlendingapp.restapi.model.CreateLoanExtensionResult;
import me.ivanovs.microlendingapp.restapi.model.CreateLoanResult;
import me.ivanovs.microlendingapp.restapi.model.LoanDTO;
import me.ivanovs.microlendingapp.restapi.model.LoanExtensionDTO;

import org.junit.Test;
import org.springframework.http.ResponseEntity;

public class LoanExtensionTest extends EmbeddedJettyTest {

    @Test
    public void loanExtensionCreateTest() {
        LoanDTO loanDTO = new LoanDTO();
        loanDTO.setAmount(new BigDecimal(100.00));
        loanDTO.setTerm(20);

        ResponseEntity<CreateLoanResult> loanResponseEntity = REST_TEMPLATE
                .postForEntity("http://localhost:9090/loan/apply", loanDTO,
                        CreateLoanResult.class, new HashMap<String, String>());
        CreateLoanResult createLoanResult = loanResponseEntity.getBody();
        LoanDTO loanResultDTO = createLoanResult.getLoanDTO();
        Long loanId = loanResultDTO.getId();

        LoanExtensionDTO extensionDTO = new LoanExtensionDTO();
        extensionDTO.setLoanId(loanId);

        ResponseEntity<CreateLoanExtensionResult> extensionResponseEntity = REST_TEMPLATE
                .postForEntity("http://localhost:9090/loan/extend/",
                        extensionDTO, CreateLoanExtensionResult.class,
                        new HashMap<String, String>());
        CreateLoanExtensionResult createExtensionResult = extensionResponseEntity.getBody();
        LoanExtensionDTO extensionResultDTO = createExtensionResult.getExtensionDTO();
        assertThat(extensionResultDTO.getId(), is(notNullValue()));
    }
}
