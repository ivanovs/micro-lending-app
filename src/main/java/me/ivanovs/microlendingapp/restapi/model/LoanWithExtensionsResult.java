package me.ivanovs.microlendingapp.restapi.model;

import java.util.List;

public class LoanWithExtensionsResult {

    private LoanDTO loanData;
    private List<LoanExtensionDTO> extensions;

    public LoanDTO getLoanData() {
        return loanData;
    }

    public void setLoanData(LoanDTO loanData) {
        this.loanData = loanData;
    }

    public List<LoanExtensionDTO> getExtensions() {
        return extensions;
    }

    public void setExtensions(List<LoanExtensionDTO> extensions) {
        this.extensions = extensions;
    }

}
