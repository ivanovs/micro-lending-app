package me.ivanovs.microlendingapp.restapi.model;

public class CreateLoanResult extends PostActionResult {

    private LoanDTO loanDTO;

    public LoanDTO getLoanDTO() {
        return loanDTO;
    }

    public void setLoanDTO(LoanDTO loanDTO) {
        this.loanDTO = loanDTO;
    }
}