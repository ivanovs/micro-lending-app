package me.ivanovs.microlendingapp.core.database;

import java.util.Date;
import java.util.List;

import me.ivanovs.microlendingapp.core.domain.Loan;

public interface LoanDAO {

    long getUserLoansCountAfterDate(String userIP, Date date);

    void create(Loan loan);

    Loan getById(Long id);

    void update(Loan loan);
    
    List<Loan> getAll();

}
