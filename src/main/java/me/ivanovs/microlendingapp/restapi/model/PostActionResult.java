package me.ivanovs.microlendingapp.restapi.model;

public abstract class PostActionResult {

    protected String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
