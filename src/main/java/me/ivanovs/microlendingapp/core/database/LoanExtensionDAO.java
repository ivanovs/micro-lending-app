package me.ivanovs.microlendingapp.core.database;

import java.util.List;

import me.ivanovs.microlendingapp.core.domain.LoanExtension;

public interface LoanExtensionDAO {

    void create(LoanExtension extension);

    LoanExtension getById(Long id);

    List<LoanExtension> getByLoanId(Long loanId);

}
