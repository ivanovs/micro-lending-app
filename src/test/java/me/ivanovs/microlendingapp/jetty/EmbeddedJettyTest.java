package me.ivanovs.microlendingapp.jetty;

import me.ivanovs.microlendingapp.config.ApplicationConfig;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

@ContextConfiguration(classes = ApplicationConfig.class)
public class EmbeddedJettyTest {

    private static EmbeddedJetty jettyServer;

    private static final int SERVER_PORT = 9090;

    protected static final RestTemplate REST_TEMPLATE = new RestTemplate();

    @BeforeClass
    public static void init() throws Exception {
        jettyServer = new EmbeddedJetty("/", SERVER_PORT);
        jettyServer.start();
    }

    @AfterClass
    public static void clean() throws Exception {
        jettyServer.stop();
        System.out.println("Sleeping for 5s...");
        Thread.sleep(5000);
    }

}
