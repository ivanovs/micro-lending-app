package me.ivanovs.microlendingapp.core.service;

import java.math.BigDecimal;
import java.util.Date;

import me.ivanovs.microlendingapp.core.database.LoanDAO;
import me.ivanovs.microlendingapp.core.database.LoanExtensionDAO;
import me.ivanovs.microlendingapp.core.domain.Loan;
import me.ivanovs.microlendingapp.core.domain.LoanExtension;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoanExtensionService {

    private final int TERM_DAYS_INCREASE_FACTOR         = 7;
    private final BigDecimal INTEREST_INCREASE_FACTOR   = new BigDecimal("1.5000");

    
    @Autowired
    private LoanDAO loanDAO;

    @Autowired
    private LoanExtensionDAO extensionDAO;

    @Transactional("hibernateTX")
    public LoanExtension extendLoan(Long loanId) throws LoanExtensionCreationException {

        Loan loan = loanDAO.getById(loanId);
        if (loan == null) {
            throw new LoanExtensionCreationException("Loan [id=" + loanId + "] does not exist");
        }
        int loanTerm = loan.getTerm();
        loan.setTerm(loanTerm + TERM_DAYS_INCREASE_FACTOR);

        BigDecimal interest = loan.getInterest();
        loan.setInterest(interest.multiply(INTEREST_INCREASE_FACTOR));

        LoanExtension extension = new LoanExtension();
        extension.setDate(new Date());
        extension.setLoan(loan);

        extensionDAO.create(extension);
        loanDAO.update(loan);

        return extension;
    }
}
