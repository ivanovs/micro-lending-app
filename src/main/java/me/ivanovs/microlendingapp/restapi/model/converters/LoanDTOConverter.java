package me.ivanovs.microlendingapp.restapi.model.converters;

import org.springframework.stereotype.Service;

import me.ivanovs.microlendingapp.core.domain.Loan;
import me.ivanovs.microlendingapp.restapi.model.LoanDTO;

@Service
public class LoanDTOConverter {

    public Loan convertFromDTO(LoanDTO loanDTO) {
        Loan loan = new Loan();
        loan.setId(loanDTO.getId());
        loan.setDate(loanDTO.getDate());
        loan.setUserIP(loanDTO.getUserIP());
        loan.setAmount(loanDTO.getAmount());
        loan.setTerm(loanDTO.getTerm());
        return loan;
    }

    public LoanDTO convertFromModel(Loan loan) {
        LoanDTO loanDTO = new LoanDTO();
        loanDTO.setId(loan.getId());
        loanDTO.setDate(loan.getDate());
        loanDTO.setUserIP(loan.getUserIP());
        loanDTO.setAmount(loan.getAmount());
        loanDTO.setTerm(loan.getTerm());
        return loanDTO;
    }
}
