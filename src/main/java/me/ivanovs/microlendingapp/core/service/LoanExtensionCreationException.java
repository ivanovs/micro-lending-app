package me.ivanovs.microlendingapp.core.service;

public class LoanExtensionCreationException extends Exception {

    private static final long serialVersionUID = 1L;

    public LoanExtensionCreationException(String message) {
        super(message);
    }

}
