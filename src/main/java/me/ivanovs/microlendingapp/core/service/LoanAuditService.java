package me.ivanovs.microlendingapp.core.service;

import java.util.List;

import me.ivanovs.microlendingapp.core.database.LoanDAO;
import me.ivanovs.microlendingapp.core.domain.Loan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoanAuditService {

    @Autowired
    private LoanDAO loanDAO;

    @Transactional("hibernateTX")
    public List<Loan> getAllLoans() {
        return loanDAO.getAll();
    }

}
