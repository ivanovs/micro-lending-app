package me.ivanovs.microlendingapp.restapi.controllers;

import java.util.List;

import me.ivanovs.microlendingapp.core.domain.Loan;
import me.ivanovs.microlendingapp.core.service.LoanAuditService;
import me.ivanovs.microlendingapp.restapi.model.LoanAuditResult;
import me.ivanovs.microlendingapp.restapi.model.converters.LoanAuditResultConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoanAuditController {

    @Autowired
    private LoanAuditService auditService;

    @Autowired
    private LoanAuditResultConverter auditResultConverter;

    @RequestMapping(method = RequestMethod.GET, value = "/audit")
    public ResponseEntity<LoanAuditResult> loanAudit() {
        List<Loan> loans = auditService.getAllLoans();
        LoanAuditResult auditResult = auditResultConverter.convertFromLoanList(loans);

        return new ResponseEntity<LoanAuditResult>(auditResult, HttpStatus.OK);
    }

}
