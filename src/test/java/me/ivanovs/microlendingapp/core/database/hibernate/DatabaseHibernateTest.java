package me.ivanovs.microlendingapp.core.database.hibernate;

import me.ivanovs.microlendingapp.config.ApplicationConfig;
import me.ivanovs.microlendingapp.core.database.LoanDAO;
import me.ivanovs.microlendingapp.core.database.LoanExtensionDAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfig.class)
@ActiveProfiles("test")
@TransactionConfiguration(transactionManager = "hibernateTX", defaultRollback = false)
public abstract class DatabaseHibernateTest {

    @Autowired
    @Qualifier("hibernateTX")
    protected PlatformTransactionManager transactionManager;

    @Autowired
    protected SessionFactory sessionFactory;

    @Autowired
    protected LoanDAO loanDAO;

    @Autowired
    protected LoanExtensionDAO loanExtensionDAO;

    @Before
    public void clean() {
        String[] tableNames = {"extensions", "loans"};
        for (final String tableName : tableNames) {
            doInTransaction(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    Session session = sessionFactory.getCurrentSession();
                    String queryString = "DELETE FROM " + tableName;
                    Query query = session.createSQLQuery(queryString);
                    query.executeUpdate();
                }
            });
        }
    }

    protected void doInTransaction(TransactionCallbackWithoutResult callbackWithoutResult) {
        TransactionTemplate txTemplate = new TransactionTemplate(transactionManager);
        txTemplate.execute(callbackWithoutResult);
    }
}
