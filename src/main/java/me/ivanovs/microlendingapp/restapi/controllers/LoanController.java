package me.ivanovs.microlendingapp.restapi.controllers;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import me.ivanovs.microlendingapp.core.domain.Loan;
import me.ivanovs.microlendingapp.core.service.LoanRiskAnalysisException;
import me.ivanovs.microlendingapp.core.service.LoanService;
import me.ivanovs.microlendingapp.restapi.model.CreateLoanResult;
import me.ivanovs.microlendingapp.restapi.model.LoanDTO;
import me.ivanovs.microlendingapp.restapi.model.converters.LoanDTOConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoanController {

    @Autowired
    private LoanDTOConverter loanDTOConverter;

    @Autowired
    private LoanService loanService;

    @RequestMapping(method = RequestMethod.POST, value = "/loan/apply")
    public ResponseEntity<CreateLoanResult> applyForLoan(@RequestBody LoanDTO loanDTO,
            HttpServletRequest request) {

        Loan loan = loanDTOConverter.convertFromDTO(loanDTO);
        loan.setDate(new Date());
        loan.setUserIP(request.getRemoteAddr());

        CreateLoanResult createLoanResult = new CreateLoanResult();
        try {
            Loan result = loanService.applyForLoan(loan);
            LoanDTO resultDTO = loanDTOConverter.convertFromModel(result);
            createLoanResult.setLoanDTO(resultDTO);
        } catch (LoanRiskAnalysisException e) {
            createLoanResult.setLoanDTO(new LoanDTO());
            createLoanResult.setErrorMessage("Risk is considered too high: " + e.getMessage());
        }

        return new ResponseEntity<CreateLoanResult>(createLoanResult, HttpStatus.CREATED);
    }
}
