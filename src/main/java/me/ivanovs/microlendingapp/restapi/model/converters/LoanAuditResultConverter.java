package me.ivanovs.microlendingapp.restapi.model.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.ivanovs.microlendingapp.core.domain.Loan;
import me.ivanovs.microlendingapp.core.domain.LoanExtension;
import me.ivanovs.microlendingapp.restapi.model.LoanAuditResult;
import me.ivanovs.microlendingapp.restapi.model.LoanDTO;
import me.ivanovs.microlendingapp.restapi.model.LoanExtensionDTO;
import me.ivanovs.microlendingapp.restapi.model.LoanWithExtensionsResult;

@Service
public class LoanAuditResultConverter {

    @Autowired
    private LoanDTOConverter loanDTOConverter;

    @Autowired
    private LoanExtensionDTOConverter extensionDTOConverter;

    public LoanAuditResult convertFromLoanList(List<Loan> loans) {
        LoanAuditResult auditResult = new LoanAuditResult();
        List<LoanWithExtensionsResult> auditEntities = new ArrayList<LoanWithExtensionsResult>();
        for (Loan loan : loans) {
            LoanDTO loanDTO = loanDTOConverter.convertFromModel(loan);
            List<LoanExtensionDTO> extensionDTOs = new ArrayList<LoanExtensionDTO>();
            if (loan.getExtensions() != null) {
                for (LoanExtension extension : loan.getExtensions()) {
                    LoanExtensionDTO extensionDTO = extensionDTOConverter.convertFromModel(extension);
                    extensionDTOs.add(extensionDTO);
                }
            }
            LoanWithExtensionsResult loanWithExtensions = new LoanWithExtensionsResult();
            loanWithExtensions.setLoanData(loanDTO);
            loanWithExtensions.setExtensions(extensionDTOs);
            auditEntities.add(loanWithExtensions);
        }
        auditResult.setAuditEntities(auditEntities);
        return auditResult;
    }
}
