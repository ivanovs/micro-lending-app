package me.ivanovs.microlendingapp.restapi.model.converters;

import me.ivanovs.microlendingapp.core.domain.LoanExtension;
import me.ivanovs.microlendingapp.restapi.model.LoanExtensionDTO;

import org.springframework.stereotype.Service;

@Service
public class LoanExtensionDTOConverter {

    public LoanExtension convertFromDTO(LoanExtensionDTO extensionDTO) {
        LoanExtension extension = new LoanExtension();
        extension.setId(extensionDTO.getId());
        extension.setDate(extensionDTO.getDate());
        return extension;
    }

    public LoanExtensionDTO convertFromModel(LoanExtension extension) {
        LoanExtensionDTO extensionDTO = new LoanExtensionDTO();
        extensionDTO.setId(extension.getId());
        extensionDTO.setDate(extension.getDate());
        return extensionDTO;
    }
}
