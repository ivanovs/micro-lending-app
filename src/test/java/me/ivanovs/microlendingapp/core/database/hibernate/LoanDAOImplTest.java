package me.ivanovs.microlendingapp.core.database.hibernate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.Date;

import me.ivanovs.microlendingapp.core.domain.Loan;

import org.junit.Test;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

public class LoanDAOImplTest extends DatabaseHibernateTest {

    @Test
    public void testCreateLoan() {
        doInTransaction(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                Loan loan = new Loan();
                loan.setDate(new Date());
                loan.setAmount(new BigDecimal("100.00"));
                loan.setInterest(new BigDecimal("1.1000"));
                loan.setTerm(30);
                loan.setUserIP("127.0.0.1");
                assertThat(loan.getId(), is(nullValue()));
                loanDAO.create(loan);
                assertThat(loan.getId(), is(notNullValue()));
            }
        });
    }
}
