package me.ivanovs.microlendingapp.core.service;

import java.math.BigDecimal;

import me.ivanovs.microlendingapp.core.database.LoanDAO;
import me.ivanovs.microlendingapp.core.domain.Loan;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoanService {

    private final BigDecimal DEFAULT_INTEREST       = new BigDecimal("1.1000");
    private final BigDecimal MAX_POSSIBLE_AMOUNT    = new BigDecimal("300.00"); 
    private final int LATE_HOURS_LIMIT              = 6;
    private final int MAX_APPS_PER_DAY_LIMIT        = 5;

    @Autowired
    private LoanDAO loanDAO;

    @Transactional("hibernateTX")
    public Loan applyForLoan(Loan loan) throws LoanRiskAnalysisException {

        checkTimeAndAmount(loan);
        checkMaxApplicationsPerUser(loan);
        
        loan.setInterest(DEFAULT_INTEREST);
        loanDAO.create(loan);
        return loan;
    }

    private void checkTimeAndAmount(Loan loan) throws LoanRiskAnalysisException {
        DateTime loanDateTime = new DateTime(loan.getDate());
        if (loanDateTime.getHourOfDay() < LATE_HOURS_LIMIT
                && MAX_POSSIBLE_AMOUNT.compareTo(loan.getAmount()) <= 0) {
            throw new LoanRiskAnalysisException(
                    "The attempt to take loan is made after 00:00 with max possible amount ("
                            + MAX_POSSIBLE_AMOUNT + ")");
        }
    }

    private void checkMaxApplicationsPerUser(Loan loan) throws LoanRiskAnalysisException {
        DateTime loanDateTime = new DateTime(loan.getDate());
        DateTime timeAtStartOfDay = loanDateTime.withTimeAtStartOfDay();
        if (loanDAO.getUserLoansCountAfterDate(loan.getUserIP(),
                timeAtStartOfDay.toDate()) > MAX_APPS_PER_DAY_LIMIT) {
            throw new LoanRiskAnalysisException("Reached max applications ("
                    + MAX_APPS_PER_DAY_LIMIT + ") per day from a single IP");
        }

    }

}
