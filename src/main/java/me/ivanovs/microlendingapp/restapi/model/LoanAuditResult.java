package me.ivanovs.microlendingapp.restapi.model;

import java.util.List;

public class LoanAuditResult {

    private List<LoanWithExtensionsResult> auditEntities;

    public List<LoanWithExtensionsResult> getAuditEntities() {
        return auditEntities;
    }

    public void setAuditEntities(List<LoanWithExtensionsResult> auditEntities) {
        this.auditEntities = auditEntities;
    }

}
