package me.ivanovs.microlendingapp.core.database.hibernate;

import java.util.List;

import me.ivanovs.microlendingapp.core.database.LoanExtensionDAO;
import me.ivanovs.microlendingapp.core.domain.LoanExtension;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoanExtensionDAOImpl implements LoanExtensionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void create(LoanExtension extension) {
        getCurrentSession().saveOrUpdate(extension);
    }

    @Override
    public LoanExtension getById(Long id) {
        return (LoanExtension) getCurrentSession().get(LoanExtension.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<LoanExtension> getByLoanId(Long loanId) {
        Criteria criteria = getCurrentSession().createCriteria(LoanExtension.class);
        criteria.add(Restrictions.eq("loanId", loanId));
        criteria.addOrder(Order.asc("date"));
        return criteria.list();
    }

}